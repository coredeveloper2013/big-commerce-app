<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BigCommerceAppController extends Controller
{

    private $store_hash;
    private $headers = [];
    private $url = '';

    private function generateHeader()
    {
        $this->store_hash = 'stores/dyhazx4rd1';
        $this->headers = [
            'X-Auth-Client' => config('services.big_commerce.client_id'),
            'X-Auth-Token' => config('services.big_commerce.auth_token'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $this->url = 'https://api.bigcommerce.com/';
    }

    public function getProducts()
    {
        $this->generateHeader();
        $products = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v3/catalog/products");
        return response($products);
    }

    public function getProduct($id)
    {
        $this->generateHeader();
        $product = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v3/catalog/products?id={$id}");
        return response($product);
    }

    public function getOrders()
    {
        $this->generateHeader();
        $orders = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v2/orders");
        return response($orders);
    }
}
