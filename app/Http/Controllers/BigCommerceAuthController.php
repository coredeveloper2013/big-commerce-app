<?php

namespace App\Http\Controllers;

use App\User;
use Bigcommerce\Api\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class BigCommerceAuthController extends Controller
{
    public function install(Request $request)
    {
        if (!$request->has('code') || !$request->has('scope') || !$request->has('context')) {
            return redirect()->action('MainController@error')->with('error_message', 'Not enough information was passed to install this app.');
        }


        $login = Http::post('https://login.bigcommerce.com/oauth2/token', [
            'client_id' => config('services.big_commerce.client_id'),
            'client_secret' => config('services.big_commerce.client_secret'),
            'redirect_uri' => 'http://127.0.0.1:8000/oauth',
            'grant_type' => 'authorization_code',
            "code" => $request->get("code"),
            "scope" => $request->get("scope"),
            "context" => $request->get("context")
        ]);


        if ($login->ok()) {
            $login_data = $login->json();
            $request->session()->put('store_hash', $login_data['context']);
            $request->session()->put('access_token', $login_data['access_token']);
            $request->session()->put('user_id', $login_data['user']['id']);
            $request->session()->put('user_email', $login_data['user']['email']);


            $user = User::where('email', $login_data['user']['email'])->first();
            if (!$user) {
                $user = new User();
            }
            $user->big_user_id = $login_data['user']['id'];
            $user->name = $login_data['user']['username'];
            $user->email = $login_data['user']['email'];
            $user->access_token = $login_data['access_token'];
            $user->store_hash = $login_data['context'];
            $user->save();

            auth()->login($user, true);
            return redirect('/');
        } else {

        }
        return $login;

    }

    public function load(Request $request)
    {
        $decoded_data = $this->verifySignedRequest($request->signed_payload);
        if ($decoded_data != null) {
            $user = User::where('big_user_id', $decoded_data['user']['id'])->firstOrFail();
            $request->header('Set-Cookie: cross-site-cookie=bar; SameSite=None; Secure', false);
            $request->session()->put('store_hash', $decoded_data['context']);
            $request->session()->put('access_token', $user->access_token);
//            auth()->login($user, true);
            return redirect()->to("/");
        }
    }

    public function midRed($id)
    {

    }

    public function uninstall(Request $request)
    {
        return $request->all();
    }

    public function error(Request $request)
    {
        $errorMessage = "Internal Application Error";

        if ($request->session()->has('error_message')) {
            $errorMessage = $request->session()->get('error_message');
        }

        echo '<h4>An issue has occurred:</h4> <p>' . $errorMessage . '</p> <a href="' . $this->baseURL . '">Go back to home</a>';
    }

    private function verifySignedRequest($signedRequest)
    {
        list($encodedData, $encodedSignature) = explode('.', $signedRequest, 2);

        // decode the data
        $signature = base64_decode($encodedSignature);
        $jsonStr = base64_decode($encodedData);
        $data = json_decode($jsonStr, true);

        // confirm the signature
        $expectedSignature = hash_hmac('sha256', $jsonStr, config('services.big_commerce.client_secret'), $raw = false);
        if (!hash_equals($expectedSignature, $signature)) {
            error_log('Bad signed request from BigCommerce!');
            return null;
        }
        return $data;
    }
}
