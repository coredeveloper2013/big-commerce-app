<?php

namespace App\Http\Middleware;

use Closure;

class P3PFixer
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('P3P', 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
        $response->header("Set-Cookie", "HttpOnly;Secure;SameSite=None");
        return $response;
    }
}
