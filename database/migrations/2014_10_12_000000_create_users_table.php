<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('big_user_id')->comment('Big commerce User ID');
            $table->string('name');
            $table->string('email')->unique()->comment('Big commerce email address');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('access_token')->comment('Big commerce access token');
            $table->string('store_hash')->comment('Big commerce store hash');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
