<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/oauth', 'BigCommerceAuthController@install');
Route::get('/load', 'BigCommerceAuthController@load');
Route::get('/uninstall', 'BigCommerceAuthController@uninstall');
Route::get('/med-red/{id}', 'BigCommerceAuthController@midRed');

Route::prefix('vue/')->group(function () {
    Route::get('/all-product', 'BigCommerceAppController@getProducts');
    Route::get('/product/{id}', 'BigCommerceAppController@getProduct');
    Route::get('/orders', 'BigCommerceAppController@getOrders');
});


Route::get('/{any}', function () {
//    return session('access_token');
    //  return response()->json(auth()->user());
    return view('app.main');
})->where('any', '.*');



